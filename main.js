const { app, BrowserWindow } = require('electron');
const isDevMode = require('electron-is-dev');
const electronReload = require('electron-reload');
const path = require('path');

if (isDevMode) {
  electronReload(__dirname, {
    electron: path.join(__dirname, 'node_modules', '.bin', 'electron'),
  });
}

let mainWindow;

const createWindow = () => {
  const browserOptions = {
    width: 1024,
    height: 600,
    // webPreferences: {
    //   webSecurity: false,
    // },
  };

  mainWindow = new BrowserWindow(browserOptions);
  mainWindow.loadURL(process.env.NODE_ENV === 'development' ? 'http://localhost:3000/' : `file://${__dirname}/dist/index.html`);

  if (isDevMode) {
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
    const {
      default: installExtension,
      REACT_DEVELOPER_TOOLS,
      REDUX_DEVTOOLS,
    } = require('electron-devtools-installer'); // eslint-disable-line
    installExtension(REACT_DEVELOPER_TOOLS)
      .then(name => console.log(`Added Extension:  ${name}`))
      .catch(err => console.log('An error occurred: ', err));

    installExtension(REDUX_DEVTOOLS)
      .then(name => console.log(`Added Extension:  ${name}`))
      .catch(err => console.log('An error occurred: ', err));
  }

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
